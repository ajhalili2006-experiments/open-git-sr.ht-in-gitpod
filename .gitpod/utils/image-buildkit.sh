#!/usr/bin/env bash
set -x

BUILD_DATE=$(date +%Y%m%d%H%M%S --utc)
IMAGE_REPO=${IMAGE_REPO:-"quay.io/gitpodified-workspace-image/hutpod-docker-env"}
IMAGE_REPO_BASE=${IMAGE_REPO_BASE:-"$IMAGE_REPO:latest"}
COMMIT_SHA=$(git rev-parse HEAD)
COMMIT_BRANCH=$(git branch --show-current | sed -e 's/[^[:alnum:]]/-/g' | tr -s '-' | tr A-Z a-z)
docker build \
    --pull \
    --file .gitpod/docker/Dockerfile \
    --cache-from "$IMAGE_REPO_BASE" \
    --tag "$IMAGE_REPO:$BUILD_DATE" \
    --tag "$IMAGE_REPO:commit-$COMMIT_SHA" \
    --tag "$IMAGE_REPO:branch-$COMMIT_BRANCH" .

if [[ $PUSH_IMAGES != "" ]]; then
  for IMAGE in $IMAGE_REPO:$BUILD_DATE $IMAGE_REPO:commit-$COMMIT_SHA $IMAGE_REPO:branch-$COMMIT_BRANCH; do
    docker push "$IMAGE" || echo "failed to push $IMAGE, skipping..."
  done
else
  echo docker push step was skipped
fi